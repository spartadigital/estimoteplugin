var exec = require('cordova/exec');

/**
 * Main exported module.
 * @namespace onyxbeacon
 */

/* constructor */
function EstimoteBeacon() {}
//var Estimote = Estimote || {};

//Check Bluetooth State
EstimoteBeacon.prototype.checkbluetoothState = function(success , error){
    exec(success,error,'Estimote','checkbluetoothState',[]);
};

// Export module.
var EstimoteBeacon = new EstimoteBeacon();
module.exports = EstimoteBeacon;

